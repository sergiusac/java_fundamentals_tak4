package epamjava.fundamentals.task4;

import java.util.Random;

public class Utilities {

    public static Matrix randomSquareMatrix(int dimension, int range) {
        Matrix matrix = new Matrix(dimension, dimension);

        Random random = new Random();
        for (int i = 0; i < matrix.getRowSize(); i++) {
            for (int j = 0; j < matrix.getColSize(); j++) {
                matrix.set(i, j, random.nextInt(2 * range + 1) - range);
            }
        }

        return matrix;
    }
}
