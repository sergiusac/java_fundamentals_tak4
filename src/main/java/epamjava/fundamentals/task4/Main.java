package epamjava.fundamentals.task4;

import java.util.Optional;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.print("Matrix dimension (N): ");
        int dimension = scan.nextInt();

        System.out.print("Range of values (M): ");
        int range = scan.nextInt();

        System.out.print("Column number to sort the rows by (k): ");
        int column = scan.nextInt();

        Matrix matrix = Utilities.randomSquareMatrix(dimension, range);

        System.out.println("Original matrix");
        System.out.println(matrix);

        System.out.println("Matrix sorted by column " + column);
        matrix.sortByColumn(column);
        System.out.println(matrix);

        System.out.println("Sum in each row: ");
        for (int i = 0; i < matrix.getRowSize(); i++) {
            System.out.print(i + ". ");

            Optional<Integer> a = matrix.getIdxOfFirstPositiveNumberInRow(i);
            Optional<Integer> b = matrix.getIdxOfSecondPositiveNumberInRow(i);

            if (a.isPresent() && b.isPresent() && b.get() - a.get() > 1) {
                int firstValue = matrix.get(i, a.get());
                int secondValue = matrix.get(i, b.get());
                System.out.print(String.format("Elements: %d, %d; ", firstValue, secondValue));

                int sum = matrix.calcSumBetweenTwoIndexes(i, a.get(), b.get());
                System.out.println(String.format("Sum: %d;", sum));
            } else {
                System.out.println("no numbers to sum");
            }
        }

        int max = matrix.getMaximumValue();
        matrix.removeColumnRowContaining(max);
        System.out.println(String.format("Matrix with removed maximum value (%d):", max));
        System.out.println(matrix);
    }
}
