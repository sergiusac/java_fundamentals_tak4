package epamjava.fundamentals.task4;

import java.util.Optional;

public class Matrix {
    private int[][] elements;
    private int rowSize, colSize;

    /**
     * Matrix constructor
     * @param rowSize specifies the number of rows
     * @param colSize specifies the number of columns
     */
    public Matrix(int rowSize, int colSize) {
        elements = new int[rowSize][colSize];
        this.rowSize = rowSize;
        this.colSize = colSize;
    }

    /**
     * Returns an element specified by the indexes `i` and `j`
     * @param i row index
     * @param j column index
     * @return an element according to the indexes
     */
    public int get(int i, int j) {
        return elements[i][j];
    }

    /**
     * Sets the value in this matrix
     * @param i row index
     * @param j column index
     * @param value specifies the value to set
     */
    public void set(int i, int j, int value) {
        elements[i][j] = value;
    }

    /**
     * Returns the number of rows
     * @return the number of rows
     */
    public int getRowSize() {
        return rowSize;
    }

    /**
     * Returns the number of columns
     * @return the number of columns
     */
    public int getColSize() {
        return  colSize;
    }

    /**
     * Finds a maximum value in this entire matrix
     * @return a maximum value
     */
    public int getMaximumValue() {
        int max = this.get(0, 0);

        for (int i = 0; i < rowSize; i++) {
            for (int j = 0; j < colSize; j++) {
                if (this.get(i, j) > max) {
                    max = this.get(i, j);
                }
            }
        }

        return max;
    }

    /**
     * Returns index of a first positive number in a specified row
     * @param row specifies a row where to find the first positive number
     * @return an optional first found positive value in a specified row
     */
    public Optional<Integer> getIdxOfFirstPositiveNumberInRow(int row) {
        Integer res = null;

        for (int i = 0; i < rowSize; i++) {
            if (this.get(row, i) > 0) {
                res = i;
                break;
            }
        }

        return Optional.ofNullable(res);
    }

    /**
     * Returns index of a second positive number in a specified row
     * @param row specifies a row where to find a second positive number
     * @return an optional second found value in a specified row
     */
    public Optional<Integer> getIdxOfSecondPositiveNumberInRow(int row) {
        Integer res = null;

        boolean flag = false;
        for (int i = 0; i < rowSize; i++) {
            if (flag && this.get(row, i) > 0) {
                res = i;
                break;
            }
            if (!flag && this.get(row, i) > 0) {
                flag = true;
            }
        }

        return Optional.ofNullable(res);
    }

    /**
     * Swaps the rows specified by the parameters `row1` and `row2`
     * @param row1 first row
     * @param row2 second row
     */
    public void swapRows(int row1, int row2) {
        int[] preservedRow = new int[rowSize];

        // copy values of the second row to the first row and preserve values of the first row
        for (int i = 0; i < rowSize; i++) {
            preservedRow[i] = this.get(row1, i);
            this.set(row1, i, this.get(row2, i));
        }

        // copy the preserved values of the first row to the second row
        for (int i = 0; i < preservedRow.length; i++) {
            this.set(row2, i, preservedRow[i]);
        }
    }

    /**
     * Sorts the rows in ascending order according to a specified column
     * @param column to sort by
     */
    public void sortByColumn(int column) {
        for (int i = 0; i < rowSize; i++) {
            for (int j = i + 1; j < colSize; j++) {
                if (this.get(i, column) > this.get(j, column)) {
                    swapRows(i, j);
                }
            }
        }
    }

    /**
     * calculates the sum of elements between `startIdx` to `endIdx` in a specified row
     * @param row where to execute the calculation
     * @param startIdx specifies the index of the left boundary element
     * @param endIdx specifies the index of the right boundary element
     * @return a calculated sum
     */
    public int calcSumBetweenTwoIndexes(int row, int startIdx, int endIdx) {
        int sum = 0;

        for (int i = startIdx + 1; i < endIdx; i++) {
            sum += this.get(row, i);
        }

        return sum;
    }

    /**
     * Removes a row from this matrix
     * @param row specifies a row for removing
     */
    public void removeRow(int row) {
        int[][] newElements = new int[rowSize - 1][colSize];

        for (int i = 0; i < row; i++) {
            for (int j = 0; j < (colSize); j++) {
                newElements[i][j] = this.get(i, j);
            }
        }

        if (row < rowSize - 1) {
            for (int i = row + 1; i < rowSize; i++) {
                for (int j = 0; j < colSize; j++) {
                    newElements[i - 1][j] = this.get(i, j);
                }
            }
        }

        elements = newElements;
        rowSize = rowSize - 1;
    }

    /**
     * Removes a column from this matrix
     * @param column specifies a column for removing
     */
    public void removeColumn(int column) {
        int[][] newElements = new int[rowSize][colSize - 1];

        for (int i = 0; i < rowSize; i++) {
            for (int j = 0; j < column; j++) {
                newElements[i][j] = this.get(i, j);
            }
        }

        if (column < colSize - 1) {
            for (int i = 0; i < rowSize; i++) {
                for (int j = column + 1; j < colSize; j++) {
                    newElements[i][j - 1] = this.get(i, j);
                }
            }
        }

        elements = newElements;
        colSize = colSize - 1;
    }

    /**
     * Removes a row and a column from this matrix
     * @param row specifies a row for removing
     * @param column specifies a column for removing
     */
    public void removeRowColumn(int row, int column) {
        removeRow(row);
        removeColumn(column);
    }

    /**
     * Removes columns and rows containing an appropriate value
     * @param value specifies the value
     */
    public void removeColumnRowContaining(int value) {
        for (int i = 0; i < rowSize; i++) {
            for (int j = 0; j < colSize; j++) {
                if (this.get(i, j) == value) {
                    removeRowColumn(i, j);
                    i = 0;
                    j = 0;
                }
            }
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        for (int[] element : elements) {
            builder.append("|");
            for (int i : element) {
                builder.append('\t').append(i).append('\t');
            }
            builder.append("|\n");
        }

        return builder.toString();
    }
}
